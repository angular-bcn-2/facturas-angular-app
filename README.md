#Formación Angular - Barcelona

##Ejercicio aplicación Facturas
Ejercicio sobre la aplicación de Bootstrap y Angular, tal y como quedó el último día de clase. Para que funcione, tiene que estar instalada y corriendo la API del
repositorio llamado facturas-api

###Cómo descargar
Si quieres usar Git, copia la URL que tienes más arriba, al lado de **HTTPS**, y haz un **git clone** desde tu disco duro.

Si quieres descargarte un zip, ve a la opción **Downloads** del menú izquierdo.

Si es la primera vez que lo descargas, o si se ha añadido algún cambio en el archivo package.json, recuerda lanzar **npm install** después de la descarga.