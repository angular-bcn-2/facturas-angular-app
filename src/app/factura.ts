export class Factura {
    constructor(
        public id: number,
        public numero: string,
        public base: number,
        public iva: number,
        public tipo: string
    ) { }

    public total(): number {
        return Number((this.base + this.totalIva()).toFixed(2));
    }

    public totalIva(): number {
        let iva: string = (this.base * (this.iva / 100)).toFixed(2);
        let ivaNumber: number = Number(iva);
        return ivaNumber;
    }
}