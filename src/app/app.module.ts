import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { ListadoComponent } from './listado/listado.component';
import { TotalesComponent } from './totales/totales.component';
import { FormularioComponent } from './formulario/formulario.component';
import { FacturasService } from './facturas.service';
import { DecimalesPipe } from './decimales.pipe';
import { InicioComponent } from './inicio/inicio.component';
import { NuevaFacturaComponent } from './nueva-factura/nueva-factura.component';
import { TotalesFacturasComponent } from './totales-facturas/totales-facturas.component';
import { NoEncontradoComponent } from './no-encontrado/no-encontrado.component';

let rutas: Routes = [
  { path: 'inicio', component: InicioComponent,  },
  { path: 'totales-facturas', component: TotalesFacturasComponent },
  { path: 'nueva-factura', component: FormularioComponent },
  { path: '', redirectTo: '/inicio', pathMatch: "full" },
  { path: '**', component: NoEncontradoComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    ListadoComponent,
    TotalesComponent,
    FormularioComponent,
    DecimalesPipe,
    InicioComponent,
    NuevaFacturaComponent,
    TotalesFacturasComponent,
    NoEncontradoComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, FormsModule, RouterModule.forRoot(rutas)
  ],
  providers: [ FacturasService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
