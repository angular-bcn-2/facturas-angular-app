import { Component, Input, OnInit } from '@angular/core';

import { FacturasService } from '../facturas.service';

@Component({
  selector: 'app-totales',
  templateUrl: './totales.component.html',
  styles: []
})
export class TotalesComponent implements OnInit {

  constructor(private facturasService: FacturasService) { }

  public getTotalPorTipo(tipo: string): number {
    return this.facturasService.getTotalPorTipo(tipo);
  }

  public getTotalBasesPorTipo(tipo: string): number {
    return this.facturasService.getTotalBasesPorTipo(tipo);
  }

  public getTotalIvasPorTipo(tipo: string): number {
    return this.facturasService.getTotalIvasPorTipo(tipo);
  }

  public getBalance(): number {
    return this.facturasService.getBalance();
  }

  public claseBalance(): string {
    return this.getBalance() >= 0 ? "positivo" : "negativo";
  }

  ngOnInit() {
  }

}
