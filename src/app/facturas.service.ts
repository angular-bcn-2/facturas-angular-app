import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Factura } from './factura';

const URLAPI: string = "http://localhost:3000/facturas/";

@Injectable()
export class FacturasService {

  public facturas: Factura[] = [];

  constructor(private http: HttpClient) {
    this.getFacturasApi();
  }

  private getFacturasApi(): void {
    this.http.get<Factura[]>(URLAPI).subscribe(
      facturas => {
        facturas.forEach(factura => {
          this.facturas.push(new Factura(factura.id, factura.numero, factura.base, factura.iva, factura.tipo));
        })
      }
    )
  }

  public getFacturasPorTipo(tipo: string): Array<Factura> {
    return this.facturas.filter(factura => factura.tipo == tipo);
  }

  public getTotalPorTipo(tipo: string): number {
    if (this.facturas.length) {
      return this.getFacturasPorTipo(tipo).map(
        factura => factura.total()
      ).reduce(
        (total1, total2) => total1 + total2
        )
    } else {
      return 0;
    }
  }

  public getTotalBasesPorTipo(tipo: string): number {
    if (this.facturas.length) {
      return this.getFacturasPorTipo(tipo).map(
        factura => factura.base
      ).reduce(
        (base1, base2) => base1 + base2
        )
    } else {
      return 0;
    }
  }

  public getTotalIvasPorTipo(tipo: string): number {
    if (this.facturas.length) {
      return this.getFacturasPorTipo(tipo).map(
        factura => factura.totalIva()
      ).reduce(
        (total1, total2) => total1 + total2
        )
    } else {
      return 0;
    }
  }

  public getBalance(): number {
    return this.getTotalPorTipo("ingreso") - this.getTotalPorTipo("gasto");
  }

  public crearFactura(factura: Factura): void {
    this.http.post<Factura>(URLAPI, factura).subscribe(
      factura => {
        this.facturas.push(new Factura(factura.id, factura.numero, factura.base, factura.iva, factura.tipo));
      }
    )
  }

}
