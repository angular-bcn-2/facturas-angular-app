import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { FacturasService } from '../facturas.service';
import { Factura } from '../factura';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styles: []
})
export class FormularioComponent implements OnInit {

  public nuevaFactura: Factura = new Factura(0, "", null, null, "");

  constructor(private facturasService: FacturasService, private title: Title) { }

  public crearFactura(formulario): void {
    this.facturasService.crearFactura(this.nuevaFactura);
    formulario.reset();
  }

  ngOnInit() {
    this.title.setTitle("Facturas - Nueva factura");
  }

}
