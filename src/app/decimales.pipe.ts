import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decimales'
})
export class DecimalesPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let nDecimales = (args && args[0]) || 1;
    return Number(value.toFixed(nDecimales));
  }

}
