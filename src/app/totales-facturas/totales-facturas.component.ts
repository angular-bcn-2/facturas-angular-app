import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-totales-facturas',
  templateUrl: './totales-facturas.component.html',
  styles: []
})
export class TotalesFacturasComponent implements OnInit {

  constructor(private router: Router, private title: Title) { }

  ngOnInit() {
    this.title.setTitle("Facturas - Totales facturas");
    setTimeout(() => {
      this.router.navigate(['/inicio']);
    }, 3000);
  }

}
