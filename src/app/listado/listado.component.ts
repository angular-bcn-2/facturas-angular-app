import { Component, Input, OnInit } from '@angular/core';

import { Factura } from '../factura';

import { FacturasService } from '../facturas.service';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styles: []
})
export class ListadoComponent implements OnInit {

  @Input()
  public tipo: string;

  public hoy = new Date();

  constructor(private facturasService: FacturasService) { }

  public getFacturas(): Factura[] {
    return this.facturasService.getFacturasPorTipo(this.tipo);
  }

  ngOnInit() {
  }

}
